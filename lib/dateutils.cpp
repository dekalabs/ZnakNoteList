#include "dateutils.hpp"
#include <regex>
#include <stdexcept>

bool
validateDate(const Date& date)
{
    if (date.size() != DATE_SIZE)
        return false;

    uint day = date[DATE_DAY];
    uint month = date[DATE_MONTH];
    uint year = date[DATE_YEAR];

    if (year > 9999) {
        return false;

    } else if (month < MONTH_BEGIN || month > MONTH_END) {
        return false;
    } else if (month == MONTH_FEBRUARY) {
        if ((year % 400) == 0 || ((year % 4) == 0 && (year % 100) != 0)) {
            //Високосный код
            if (day < 1 || day > daysCountInMonth[MONTH_FEBRUARY] + 1) {
                return false;
            }
        } else {
            // не високосный код
            if (day < 1 || day > daysCountInMonth[MONTH_FEBRUARY]) {

                return false;
            }
        }
    } else if (day < 1 || day > daysCountInMonth[month]) {
        return false;
    } else
        return true;
    return true;
}

String
dateToString(const Date& date)
{
    if (!validateDate(date))
        return "";

    StringStream SS;
    SS << date[DATE_YEAR] << "-" << date[DATE_MONTH] + 1 << "-"
       << date[DATE_DAY];
    return SS.str();
}

Date
stringToDate(const String& str)
{
    auto regexp = std::regex("\\d{1,4}-\\d{1,2}-\\d{1,2}");
    std::smatch result;
    std::regex_match(str, result, regexp);

    if (result.empty()) {
        throw std::invalid_argument(
          "Нельзя сконвертировать строку в дату : Дата имеет неверный формат");
    }

    auto posYear = str.find('-');
    uint year = static_cast<uint>(std::stoi(str.substr(0, posYear)));
    auto posMonth = str.find('-', posYear + 1);
    uint month = static_cast<uint>(
      std::stoi(str.substr(posYear + 1, posMonth - posYear)) - 1);
    uint day = static_cast<uint>(std::stoi(str.substr(posMonth + 1)));

    Date date = { day, month, year };

    if (!validateDate(date)) {
        throw std::invalid_argument(
          "Нельзя сконвертировать строку в дату : строка имеет верный формат, "
          "но содержит недопустимые значения.");
    }

    return date;
}

int
dateCompare(const Date& check, const Date& other)
{
    if (!validateDate(check) || !validateDate(other)) {
        throw std::invalid_argument(
          "isDateLater: нельзя сравнивать даты, если они некорректные");
    }

    if (check[DATE_YEAR] > other[DATE_YEAR]) {
        return 1;
    } else if (check[DATE_YEAR] < other[DATE_YEAR]) {
        return -1;
    }
    //Годы равны
    if (check[DATE_MONTH] > other[DATE_MONTH]) {
        return 1;
    } else if (check[DATE_MONTH] < other[DATE_MONTH]) {
        return -1;
    }
    //Месяцы равны
    if (check[DATE_DAY] > other[DATE_DAY]) {
        return 1;
    } else if (check[DATE_DAY] < other[DATE_DAY]) {
        return -1;
    }
    return 0; //< Даты одинаковы
}

bool
dateInRange(const Date& check, const Date& first, const Date& second)
{
    if (!validateDate(check) || !validateDate(first) || !validateDate(second)) {
        throw std::invalid_argument(
          "dateInRange : Дата имеет не допустимые значения");
    }
    if (dateCompare(first, second) > 0) {
        throw std::invalid_argument(
          "dateInRange : Нижний предел больше второго");
    }

    if (dateCompare(check, first) < 0)
        return false;
    if (dateCompare(check, second) > 0)
        return false;
    return true;
}

Zodiak
zodiakByDate(const Date& date)
{
    if (!validateDate(date)) {
        throw std::invalid_argument(
          "zodiakByDate : дата имеет неверный формат");
    }

    uint currentYear = date[DATE_YEAR];

    if (dateInRange(date,
                    { 21, MONTH_MARCH, currentYear },
                    { 19, MONTH_APRIL, currentYear })) {
        return ZODIAK_ARIES;
    } else if (dateInRange(date,
                           { 20, MONTH_APRIL, currentYear },
                           { 20, MONTH_MAY, currentYear })) {
        return ZODIAK_TAURUS;
    } else if (dateInRange(date,
                           { 21, MONTH_MAY, currentYear },
                           { 20, MONTH_JUNE, currentYear })) {
        return ZODIAK_GEMINI;
    } else if (dateInRange(date,
                           { 21, MONTH_JUNE, currentYear },
                           { 22, MONTH_JULE, currentYear })) {
        return ZODIAK_CANCER;
    } else if (dateInRange(date,
                           { 23, MONTH_JULE, currentYear },
                           { 22, MONTH_AUGUST, currentYear })) {
        return ZODIAK_LEO;
    } else if (dateInRange(date,
                           { 23, MONTH_AUGUST, currentYear },
                           { 22, MONTH_SEPTEMBER, currentYear })) {
        return ZODIAK_VIRGO;
    } else if (dateInRange(date,
                           { 23, MONTH_SEPTEMBER, currentYear },
                           { 22, MONTH_OCTOBER, currentYear })) {
        return ZODIAK_LIBRA;
    } else if (dateInRange(date,
                           { 23, MONTH_OCTOBER, currentYear },
                           { 21, MONTH_NOVEMBER, currentYear })) {
        return ZODIAK_SCORPIO;
    } else if (dateInRange(date,
                           { 22, MONTH_NOVEMBER, currentYear },
                           { 21, MONTH_DECEMBER, currentYear })) {
        return ZODIAK_SAGGITARIUS;
    } else if (dateInRange(date,
                           { 22, MONTH_DECEMBER, currentYear },
                           { daysCountInMonth[MONTH_DECEMBER],
                             MONTH_DECEMBER,
                             currentYear }) ||
               dateInRange(date,
                           { 1, MONTH_JANUARY, currentYear },
                           { 19, MONTH_JANUARY, currentYear })) {
        return ZODIAK_CAPRICON;
    } else if (dateInRange(date,
                           { 20, MONTH_JANUARY, currentYear },
                           { 18, MONTH_FEBRUARY, currentYear })) {
        return ZODIAK_AQUARIUS;
    } else if (dateInRange(date,
                           { 19, MONTH_FEBRUARY, currentYear },
                           { 20, MONTH_MARCH, currentYear })) {
        return ZODIAK_PISCES;
    }
    throw std::invalid_argument(
      "zodiakByDate : во время обработки произошла критическая ошибка");
}

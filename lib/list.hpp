#ifndef CYCLE_LIST
#define CYCLE_LIST

#include <stdexcept>

typedef long long offset_type;

template<class TypeData>
struct Node
{
    TypeData data;
    Node* next = nullptr;
    Node* back = nullptr;
};

template<class TypeData>
class CycleList
{
  private:
    Node<TypeData>* m_begin;

  public:
    /**
     * @brief CycleList
     * Создает пустой список
     */
    CycleList();
    /**
     * @brief CycleList копирует элементы из other
     * @param other список с элементами для копирования
     */
    CycleList(const CycleList& other);
    ~CycleList();

    /**
     * @brief operator [] получает доступ к элементу
     * @param offset положение относительно начального элемента
     * @return ссылку на запрошенный элемент
     */
    TypeData& operator[](offset_type offset) const;
    /**
     * @brief operator = присваивание с копированием элементов из rvalue
     * @param right список справа от =
     * @return
     */
    CycleList<TypeData>& operator=(const CycleList<TypeData>& right);

    /**
     * @brief push_back добавляет элемент в конец
     * @param new_data элемент для добавления
     */
    void push_back(const TypeData& new_data);
    /**
     * @brief remove удаляет элемент
     * @param offset положение относительно начального элемента
     * @details Если offset = 0, то начальный элемент будет удален,
     * а новым стартовым элементом станет следующий
     */
    void remove(offset_type offset);
    /**
     * @brief clear удаляет все элементы
     */
    void clear();

    /**
     * @brief size вовращает число элементов
     * @return число элементов в списке
     */
    size_t size() const;
    /**
     * @brief empty проверяет отсутвие элементов в списке
     * @return true - если элементов нет, иначе false
     */
    bool empty() const { return !m_begin; }
};

// Realization

template<class TypeData>
CycleList<TypeData>::CycleList()
  : m_begin(nullptr)
{}

template<class TypeData>
CycleList<TypeData>::CycleList(const CycleList<TypeData>& other)
  : m_begin(nullptr)
{
    for (int i = 0; i < other.size(); i++) {
        TypeData& new_data = other.at(i);
        this->push_back(new_data);
    }
}

template<class TypeData>
CycleList<TypeData>::~CycleList()
{
    this->clear();
}

template<class TypeData>
TypeData& CycleList<TypeData>::operator[](offset_type offset) const
{
    if (!m_begin)
        throw std::runtime_error("Попытка получить значение из пустого списка");
    auto currentNode = m_begin;
    if (offset > 0) {
        for (offset_type i = 0; i < offset; i++) {
            if (currentNode->next == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->next;
        }
    } else if (offset < 0) {

        for (offset_type i = 0; offset < i; i--) {
            if (currentNode->back == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->back;
        }
    } else
        return m_begin->data;
    return currentNode->data;
}

template<class TypeData>
CycleList<TypeData>&
CycleList<TypeData>::operator=(const CycleList<TypeData>& left)
{
    this->clear();
    for (int i = 0; i < left.size(); i++) {
        TypeData new_data = left[i];
        this->push_back(new_data);
    }
    return *this;
}

template<class TypeData>
void
CycleList<TypeData>::push_back(const TypeData& new_data)
{
    if (!m_begin) {
        auto new_node = new Node<TypeData>();
        new_node->data = new_data;
        new_node->next = new_node;
        new_node->back = new_node;
        m_begin = new_node;
    } else {
        auto new_node = new Node<TypeData>();
        new_node->data = new_data;
        new_node->next = m_begin;
        new_node->back = m_begin->back;
        m_begin->back->next = new_node;
        m_begin->back = new_node;
    }
}

template<class TypeData>
void
CycleList<TypeData>::remove(offset_type offset)
{
    if (!m_begin)
        return;

    auto currentNode = m_begin;
    if (offset > 0) {
        for (offset_type i = 0; i < offset; i++) {
            if (currentNode->next == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->next;
        }
    } else if (offset < 0) {
        for (offset_type i = 0; offset < i; i--) {
            if (currentNode->back == nullptr)
                throw std::runtime_error("CycleList сломан");
            currentNode = currentNode->back;
        }
    } else {
        currentNode = m_begin;
    }

    if (currentNode == m_begin) {
        if (currentNode->next == m_begin) { ///< Удаляется последний
            m_begin = nullptr;
        } else {
            m_begin = currentNode->next;
        }
    }

    auto lastNode = currentNode->back;
    auto nextNode = currentNode->next;
    lastNode->next = nextNode;
    nextNode->back = lastNode;
    delete currentNode;
}

template<class TypeData>
void
CycleList<TypeData>::clear()
{
    //Начинаем с конца, чтобы не изменять m_begin постоянно
    for (offset_type i = this->size() - 1; i >= 0; i--) {
        this->remove(i);
    }
}

template<class TypeData>
size_t
CycleList<TypeData>::size() const
{
    if (!m_begin)
        return 0;
    size_t size = 1;
    auto currentNode = m_begin;
    while (currentNode->next != m_begin) {
        currentNode = currentNode->next;
        size++;
    }
    return size;
}

#endif //! CYCLE_LIST

#ifndef NOTELIST_DATABASE
#define NOTELIST_DATABASE

#include "basic_utils.hpp"
#include "noteznak.hpp"
#include <sqlite3.h>

#define DEFAULT_FILE "notelist.db"
#define DEFAULT_TABLE "notelist"
#define MAX_CHARS_FULLNAME 120

static int
SQLite3_request_Callback(void* outputVector,
                         int argc,
                         char* argv[],
                         char* columnNames[]);

class Database
{
  private:
    sqlite3* m_datebase; ///< база данных

    NoteList m_znaks; ///< Временный список

  public:
    /**
     * @brief Database открывает БД из файла
     * @param file файл БД
     */
    Database(const String& file = DEFAULT_FILE);
    Database(const Database& other) = delete;
    ~Database();

    /**
     * @brief addNote добавляет новую запись в БД
     * @param fullname полное имя
     * @param znak идентификатор знака зодиака
     * @param date дата рождения
     * @throws std::invalid_argument если имя слишком длинное или идентификатор
     * знака зодиака не верный, или дата некорректна
     */
    void addNote(const String& fullname, const Zodiak& znak, const Date& date);
    /**
     * @brief editNote редактирует существующую запись
     * @param id идентификатор записи
     * @param new_fullname новое имя, может быть nullptr, тогда оно останется
     * неизменным
     * @param new_znak новый знак зодиака, может быть nullptr
     * @param new_date новая дата рождения, может быть nullptr
     * @throws std::invalid_argument если имя слишком длинное или идентификатор
     * знака зодиака не верный, или дата некорректна
     */
    void editNote(NoteID id,
                  const String* new_fullname = nullptr,
                  const Zodiak* new_znak = nullptr,
                  const Date* new_date = nullptr);
    /**
     * @brief removeNote удаляет запись по идентификатору
     * @param id идентификатор записи для удаления
     */
    void removeNote(NoteID id);

    /**
     * @brief Get получает записи сортируя их
     * @param outputList список для записи результатов
     * @param sortingField поле сортировки
     * @param isDecreasing сортировать по убыванию?
     */
    void Get(NoteList& outputList,
             ZnakFieldIDs sortingField = ZnakFieldID_ID,
             bool isDecreasing = false);

    /**
     * @brief Search ищет элементы
     * @param outputList список для записи результатов
     * @param requestField поле, которое запрашивается
     * @param request запрос(подразумевается, что корректность поля уже
     * проверена)
     * @param sortingField поле сортировки
     * @param isDecreasing сортировать по убыванию?
     */
    void Search(NoteList& outputList,
                ZnakFieldIDs requestField,
                const String& request,
                ZnakFieldIDs sortingField = ZnakFieldID_ID,
                bool isDecreasing = false);

    /**
     * @brief Purge удаляет все записи
     */
    void Purge();

    /**
     * @brief isNull проверяет является ли БД не действительной
     * @return true БД не работаспособна
     */
    bool isNull() const { return m_datebase == nullptr; }

    /**
     * @brief contains проверяет наличие элемента с определенными полями
     * @param fullname имя для поиска
     * @param date дата для поиска
     * @return существует ли такой элемент в БД
     */
    bool contains(const String& fullname, const Date& date);

  private:
    void sendRequest(const String& request, NoteList* list = nullptr);
};

#endif //! NOTELIST_DATABASE

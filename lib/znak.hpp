#ifndef NOTELIST_ZNAK
#define NOTELIST_ZNAK

enum Zodiak
{
    ZODIAK_BEGIN = 0,
    ZODIAK_ARIES = 0,   ///< Овен
    ZODIAK_TAURUS,      ///< Телец
    ZODIAK_GEMINI,      ///< Близнецы
    ZODIAK_CANCER,      ///< Рак
    ZODIAK_LEO,         ///< Лев
    ZODIAK_VIRGO,       ///< Дева
    ZODIAK_LIBRA,       ///< Весы
    ZODIAK_SCORPIO,     ///< Скорпион
    ZODIAK_SAGGITARIUS, ///< Стрелец
    ZODIAK_CAPRICON,    ///< Козерог
    ZODIAK_AQUARIUS,    ///< Водолей
    ZODIAK_PISCES,      ///< Рыбы
    ZODIAK_END = ZODIAK_PISCES,

    ZODIAK_MAX
};

static const char* znakNames[ZODIAK_MAX] = { "Овен",    "Телец",    "Близнецы",
                                             "Рак",     "Лев",      "Дева",
                                             "Весы",    "Скорпион", "Стрелец",
                                             "Козерог", "Водолей",  "Рыбы" };

#endif //! NOTELIST_ZNAK

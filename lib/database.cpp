#include "database.hpp"

int
SQLite3_request_Callback(void* outputVector,
                         int argc,
                         char* argv[],
                         char* columnNames[])
{
    if (outputVector == nullptr)
        return 0;
    NoteList* outList = static_cast<NoteList*>(outputVector);
    ZNAK new_znak;

    for (int i = 0; i < argc; i++) {

        if (strcmp(columnNames[i], fieldName[ZnakFieldID_ID]) == 0) {
            new_znak.id = static_cast<NoteID>(std::stoi(argv[i]));
        }
        if (strcmp(columnNames[i], fieldName[ZnakFieldID_FULLNAME]) == 0) {
            new_znak.fullname = argv[i];
        }
        if (strcmp(columnNames[i], fieldName[ZnakFieldID_ZNAK]) == 0) {
            new_znak.znak = static_cast<Zodiak>(std::stoi(argv[i]));
        }
        if (strcmp(columnNames[i], fieldName[ZnakFieldID_DATE]) == 0) {
            new_znak.date = stringToDate(argv[i]);
        }
    }

    outList->push_back(std::move(new_znak));

    return 0;
}

Database::Database(const String& file)
{
    int code = sqlite3_open(file.c_str(), &m_datebase);
    if (code != SQLITE_OK) {
        std::cerr << "Cannot connect to database : "
                  << sqlite3_errmsg(m_datebase) << std::endl;
        throw std::runtime_error("Не возможно открыть БД");
    }

    StringStream SS_DB_Request;
    SS_DB_Request << "CREATE TABLE IF NOT EXISTS " << DEFAULT_TABLE << " (";
    SS_DB_Request << fieldName[ZnakFieldID_ID] << " INTEGER PRIMARY KEY, ";
    SS_DB_Request << fieldName[ZnakFieldID_FULLNAME] << " VARCHAR("
                  << MAX_CHARS_FULLNAME << "), ";
    SS_DB_Request << fieldName[ZnakFieldID_ZNAK] << " INTEGER, ";
    SS_DB_Request << fieldName[ZnakFieldID_DATE] << " DATE";
    SS_DB_Request << ");";

    this->sendRequest(SS_DB_Request.str());
}

Database::~Database()
{
    m_znaks.clear();
    sqlite3_close(m_datebase);
    m_datebase = nullptr;
}

void
Database::addNote(const String& fullname, const Zodiak& znak, const Date& date)
{
    if (fullname.size() > MAX_CHARS_FULLNAME)
        throw std::invalid_argument(
          "addNote : попытка добавить слишком длинное имя");
    if (znak < ZODIAK_BEGIN || ZODIAK_END < znak)
        throw std::invalid_argument(
          "addNote : идентификатор зодиака установлен не верно");
    if (!validateDate(date)) {
        throw std::invalid_argument(
          "addNote : попытка добавить некорректную дату");
    }
    StringStream SS_DB_Request;
    SS_DB_Request << "INSERT INTO " << DEFAULT_TABLE << " ("
                  << fieldName[ZnakFieldID_FULLNAME] << ", "
                  << fieldName[ZnakFieldID_ZNAK] << ", "
                  << fieldName[ZnakFieldID_DATE] << ") "
                  << "VALUES('" << fullname << "', " << znak << ", '"
                  << dateToString(date) << "' );";

    this->sendRequest(SS_DB_Request.str());
}

void
Database::editNote(NoteID id,
                   const String* new_fullname,
                   const Zodiak* new_znak,
                   const Date* new_date)
{
    if (new_fullname && new_fullname->size() > MAX_CHARS_FULLNAME)
        throw std::invalid_argument(
          "editNote : попытка изменить на слишком длинное имя");
    if (new_znak && (*new_znak < ZODIAK_BEGIN || ZODIAK_END < *new_znak))
        throw std::invalid_argument(
          "editNote : идентификатор зодиака установлен не верно");
    if (new_date && !validateDate(*new_date)) {
        throw std::invalid_argument("editNote : новая дата не верная");
    }

    StringStream SS_DB_request;
    SS_DB_request << "UPDATE " << DEFAULT_TABLE << " SET ";

    if (new_fullname)
        SS_DB_request << fieldName[ZnakFieldID_FULLNAME] << " = '"
                      << *new_fullname << "'";
    if (new_fullname && new_znak)
        SS_DB_request << ", ";
    if (new_znak)
        SS_DB_request << fieldName[ZnakFieldID_ZNAK] << " = " << *new_znak;
    if (new_znak && new_date)
        SS_DB_request << ", ";
    if (new_date)
        SS_DB_request << fieldName[ZnakFieldID_DATE] << " = '"
                      << dateToString(*new_date) << "'";

    SS_DB_request << " WHERE " << fieldName[ZnakFieldID_ID] << " = " << id
                  << ";";

    this->sendRequest(SS_DB_request.str());
}

void
Database::removeNote(NoteID id)
{
    StringStream SS_DB_request;
    SS_DB_request << "DELETE FROM " << DEFAULT_TABLE << " WHERE "
                  << fieldName[ZnakFieldID_ID] << " = " << id << ";";

    this->sendRequest(SS_DB_request.str());
}

void
Database::Get(NoteList& outputList,
              ZnakFieldIDs sortingField,
              bool isDecreasing)
{
    if (sortingField < ZnakFieldID_BEGIN || ZnakFieldID_END < sortingField)
        throw std::invalid_argument("Get : Указано не верное поле сортировки");

    //Обработка частных случаев
    sortingField =
      (sortingField == ZnakFieldID_DAY) ? ZnakFieldID_DATE : sortingField;
    sortingField =
      (sortingField == ZnakFieldID_MONTH) ? ZnakFieldID_DATE : sortingField;
    sortingField =
      (sortingField == ZnakFieldID_YEAR) ? ZnakFieldID_DATE : sortingField;

    m_znaks.clear();

    StringStream SS_DB_Request;
    SS_DB_Request << "SELECT * FROM " << DEFAULT_TABLE << " ";
    SS_DB_Request << "ORDER BY " << fieldName[sortingField] << " ";
    if (isDecreasing)
        SS_DB_Request << "DESC;";
    else
        SS_DB_Request << "ASC;";

    this->sendRequest(SS_DB_Request.str(), &m_znaks);

    outputList = m_znaks;
}

void
Database::Search(NoteList& outputList,
                 ZnakFieldIDs requestField,
                 const String& request,
                 ZnakFieldIDs sortingField,
                 bool isDecreasing)
{
    if (sortingField < ZnakFieldID_BEGIN || ZnakFieldID_END < sortingField)
        throw std::invalid_argument("Get : Указано не верное поле сортировки");
    if (requestField < ZnakFieldID_BEGIN || ZnakFieldID_END < requestField)
        throw std::invalid_argument("Get : Указано не верное поле запроса");

    m_znaks.clear();
    StringStream SS_DB_Request;
    SS_DB_Request << "SELECT * FROM " << DEFAULT_TABLE << " ";
    if (requestField == ZnakFieldID_DAY) {
        SS_DB_Request << "WHERE " << fieldName[ZnakFieldID_DATE] << " LIKE ";
        SS_DB_Request << "'%-%-" << request << "'";
    } else if (requestField == ZnakFieldID_MONTH) {
        SS_DB_Request << "WHERE " << fieldName[ZnakFieldID_DATE] << " LIKE ";
        SS_DB_Request << "'%-" << request << "-%'";
    } else if (requestField == ZnakFieldID_YEAR) {
        SS_DB_Request << "WHERE " << fieldName[ZnakFieldID_DATE] << " LIKE ";
        SS_DB_Request << "'" << request << "-%-%'";
    } else {
        SS_DB_Request << "WHERE " << fieldName[requestField] << " LIKE ";
        SS_DB_Request << "'" << request << "'";
    }

    SS_DB_Request << " ORDER BY " << fieldName[sortingField] << " ";
    if (isDecreasing)
        SS_DB_Request << "DESC;";
    else
        SS_DB_Request << "ASC;";

    this->sendRequest(SS_DB_Request.str(), &m_znaks);

    outputList = m_znaks;
}

void
Database::Purge()
{
    m_znaks.clear();
    StringStream SS_DB_Request;
    SS_DB_Request << "DELETE FROM " << DEFAULT_TABLE << ";";

    this->sendRequest(SS_DB_Request.str());
}

bool
Database::contains(const String& fullname, const Date& date)
{
    if (!validateDate(date))
        throw std::invalid_argument("contains : дата имеет неверный формат");

    m_znaks.clear();
    StringStream SS_DB_Request;
    SS_DB_Request << "SELECT * FROM " << DEFAULT_TABLE << " WHERE "
                  << fieldName[ZnakFieldID_FULLNAME] << " = '" << fullname
                  << "' AND " << fieldName[ZnakFieldID_DATE] << " = '"
                  << dateToString(date) << "' ;";

    this->sendRequest(SS_DB_Request.str(), &m_znaks);
    if (m_znaks.size() > 0)
        return true;
    return false;
}

void
Database::sendRequest(const String& request, NoteList* list)
{
#ifndef NDEBUG
    std::cout << "[DEBUG] Processing request : " << request << "\n";
#endif // !DEBUG
    int code = sqlite3_exec(
      m_datebase, request.c_str(), SQLite3_request_Callback, list, nullptr);

    if (code != SQLITE_OK) {
#ifndef NDEBUG
        std::cerr << "Error when proccesing request : "
                  << sqlite3_errmsg(m_datebase) << std::endl
                  << "\tRequest is " << request << std::endl;
#else
        StringStream SS;
        SS << "Error when proccesing request : " << sqlite3_errmsg(m_datebase)
           << std::endl
           << "\tRequest is " << request << std::endl;
        throw std::runtime_error(SS.str());
#endif // !NDEBUG
    }
}

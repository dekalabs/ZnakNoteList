#ifndef NOTELIST_DATEUTILS
#define NOTELIST_DATEUTILS

#include "basic_utils.hpp"
#include "znak.hpp"

typedef std::vector<uint> Date;

enum DatePositions
{
    DATE_DAY = 0,
    DATE_MONTH,
    DATE_YEAR,

    DATE_SIZE
};

enum Months
{
    MONTH_BEGIN = 0,

    MONTH_JANUARY = 0,
    MONTH_FEBRUARY,
    MONTH_MARCH,
    MONTH_APRIL,
    MONTH_MAY,
    MONTH_JUNE,
    MONTH_JULE,
    MONTH_AUGUST,
    MONTH_SEPTEMBER,
    MONTH_OCTOBER,
    MONTH_NOVEMBER,
    MONTH_DECEMBER,

    MONTH_END = MONTH_DECEMBER,
    MONTH_SIZE
};

static const uint daysCountInMonth[MONTH_SIZE] = { 31, 28, 31, 30, 31, 30,
                                                   31, 31, 30, 31, 30, 31 };

/**
 * @brief validateDate проверяет на корректность дату
 * @param date дата для проверки
 * @return true если дата имеет допустимый формат
 * @details Дата считается нормальной, если размер массива равен 3.
 * Такая дата теоретически существует в григорианском календаре
 */
bool
validateDate(const Date& date);

/**
 * @brief dateToString конвертирует дату в строковое представление
 * @param date дата для конвертирования
 * @return строку в формате ГГГГ-ММ-ДД. Если дата неверная, то возвращает пустую
 * строку
 */
String
dateToString(const Date& date);

/**
 * @brief stringToDate конвертирует строку в дату
 * @param str - строка формата ГГГГ-ММ-ДД
 * @return дату со значениями из строки
 * @throws std::invalid_argument если строка имеет не верный формат или
 * недопустимые значения
 */
Date
stringToDate(const String& str);

/**
 * @brief dateCompare сравнивает дату с другой
 * @param check дата для проверки
 * @param other дата, с которой сравнивают
 * @return <0, если раньше даты проверки; =0, если одинаковы; >0 позже;
 * @throws std::invalid_argument если даты некорректные
 */
int
dateCompare(const Date& check, const Date& other);

/**
 * @brief dateInRange проверяет, что дата находится между двумя другими, включая
 * их
 * @param check дата для проверки
 * @param first нижний(ранний) предел
 * @param second верхний(поздний) предел
 * @return true, если дата для проверки лежит между пределами
 * @throws std::invalid_argument если даты(1 или более) неверны или нижний
 * предел позже верхнего
 */
bool
dateInRange(const Date& check, const Date& first, const Date& second);

/**
 * @brief zodiakByDate получает знак зодиака по дате
 * @param date дата
 * @return Идентификатор знака зодиака, который соответствует дате
 */
Zodiak
zodiakByDate(const Date& date);

#endif //! NOTELIST_DATEUTILS

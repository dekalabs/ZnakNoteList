#ifndef NOTELIST_NOTEZNAK
#define NOTELIST_NOTEZNAK

#include "basic_utils.hpp"
#include "dateutils.hpp"
#include "list.hpp"
#include "znak.hpp"

typedef uint NoteID;

enum ZnakFieldIDs
{
    ZnakFieldID_BEGIN = 0,
    ZnakFieldID_FULLNAME = 0,
    ZnakFieldID_ZNAK,
    ZnakFieldID_DATE,

    ZnakFieldID_DAY,
    ZnakFieldID_MONTH,
    ZnakFieldID_YEAR,

    ZnakFieldID_ID,
    ZnakFieldID_END = ZnakFieldID_ID,

    ZnakFieldID_MAX
};

static const char* fieldName[ZnakFieldID_MAX] = { "fullname", "znak",  "date",
                                                  "day",      "month", "year",
                                                  "noteid" };

struct ZNAK
{
    NoteID id = 0; ///< Так как другие поля могут повторяться у каждой записи
                   ///< должен быть идентификатор

    String fullname = "";                   ///< Фамилия, имя
    Zodiak znak = ZODIAK_CAPRICON;          ///< Знак зодиака
    Date date = { 1, MONTH_JANUARY, 2000 }; ///< Дата
};

typedef CycleList<ZNAK> NoteList;

#endif //! NOTELIST_NOTEZNAK

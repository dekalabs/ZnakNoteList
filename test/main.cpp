
#include "database.hpp"
#include "dateutils.hpp"
#include "znak.hpp"
#include <stdexcept>
#define assert(expr)                                                           \
    {                                                                          \
        if (!(expr)) {                                                         \
            std::cout << std::endl                                             \
                      << "Assertion failed: " << #expr << " file " << __FILE__ \
                      << " line " << __LINE__ << std::endl                     \
                      << std::endl;                                            \
        }                                                                      \
    }

using namespace std;

int
checkDateUtils();

int
checkDatabase();

int
main()
{
    int code = 0;
    int new_code = checkDateUtils();
    if (new_code > code)
        code = new_code;

    new_code = checkDatabase();
    if (new_code > code)
        code = new_code;

    if (code > 0)
        throw std::runtime_error("Error in tests");

    return code;
}

int
checkDateUtils()
{
    cout << "Testing Date Utils\n";
    //Проверка validateDate
    cout << "Testing Date Utils : validateDate\n";
    Date current = { 10, MONTH_NOVEMBER, 2000 };
    bool result;
    result = validateDate(current);
    assert(result == true);

    current = { 10, 90, 2000 };
    result = validateDate(current);
    assert(result == false);

    current = { 0, 10, 2000 };
    result = validateDate(current);
    assert(result == false);

    current = { 29, MONTH_FEBRUARY, 800 }; //Вискокосный
    result = validateDate(current);
    assert(result == true);

    current = { 29, MONTH_FEBRUARY, 804 }; //Вискокосный
    result = validateDate(current);
    assert(result == true);

    current = { 29, MONTH_FEBRUARY, 1900 }; // НЕ Вискокосный
    result = validateDate(current);
    assert(result == false);

    current = { 0, MONTH_JULE, 1900 };
    result = validateDate(current);
    assert(result == false);

    current = { 29, 2 };
    result = validateDate(current);
    assert(result == false);

    cout << "Testing Date Utils : validateDate : Ended\n";

    cout << "Testing Date Utils : dateToString\n";
    Date date = { 19, MONTH_FEBRUARY, 3129 };
    String stringDate = dateToString(date);
    assert(stringDate == "3129-2-19");

    date = { 30, MONTH_FEBRUARY, 2000 };
    stringDate = dateToString(date);
    assert(stringDate == "");
    cout << "Testing Date Utils : dateToString : Ended\n";

    cout << "Testing Date Utils : stringToDate\n";

    stringDate = "1999-02-01";
    date = stringToDate(stringDate);
    assert(date[DATE_DAY] == 1 && date[DATE_MONTH] == MONTH_FEBRUARY &&
           date[DATE_YEAR] == 1999);
    try {
        stringDate = "1999-02-222";
        Date date = stringToDate(stringDate);
        return 2;
    } catch (std::invalid_argument e) {
        //Все хорошо.
    }

    try {
        stringDate = "errored string";
        Date date = stringToDate(stringDate);
        return 3;
    } catch (std::invalid_argument e) {
        //Все хорошо.
    }

    cout << "Testing Date Utils : stringToDate : Ended\n";

    cout << "Testing Date Utils : dateCompare\n";

    assert(dateCompare({ 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_JANUARY, 2009 }) == 0);

    assert(dateCompare({ 10, MONTH_JANUARY, 2009 },
                       { 11, MONTH_JANUARY, 2009 }) < 0);
    assert(dateCompare({ 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_DECEMBER, 2009 }) < 0);
    assert(dateCompare({ 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_JANUARY, 2010 }) < 0);
    assert(dateCompare({ 10, MONTH_JANUARY, 2009 },
                       { 15, MONTH_DECEMBER, 2010 }) < 0);

    assert(dateCompare({ 11, MONTH_JANUARY, 2009 },
                       { 10, MONTH_JANUARY, 2009 }) > 0);
    assert(dateCompare({ 10, MONTH_DECEMBER, 2009 },
                       { 10, MONTH_JANUARY, 2009 }) > 0);
    assert(dateCompare({ 10, MONTH_JANUARY, 2010 },
                       { 10, MONTH_JANUARY, 2009 }) > 0);
    assert(dateCompare({ 15, MONTH_DECEMBER, 2010 },
                       { 10, MONTH_JANUARY, 2009 }) > 0);

    try {
        assert(dateCompare({ 15, 2010 }, { 10, MONTH_JANUARY, 2009 }) > 0);
        assert(dateCompare({ 15, MONTH_DECEMBER, 2010 },
                           { 29, MONTH_FEBRUARY, 2009 }) > 0);
        return 4;
    } catch (std::invalid_argument e) {
        // all is ok
    }
    try {
        assert(dateCompare({ 15, MONTH_DECEMBER, 2010 },
                           { 29, MONTH_FEBRUARY, 2009 }) < 0);
        return 5;
    } catch (std::invalid_argument e) {
        // all is ok
    }

    cout << "Testing Date Utils : dateCompare : Ended\n";

    cout << "Testing Date Utils : dateInRange\n";

    assert(dateInRange({ 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_JANUARY, 2009 }) == true);

    assert(dateInRange({ 10, MONTH_JANUARY, 2009 },
                       { 9, MONTH_JANUARY, 2009 },
                       { 11, MONTH_APRIL, 2009 }) == true);

    assert(dateInRange({ 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_APRIL, 2009 },
                       { 10, MONTH_MAY, 2009 }) == false);

    assert(dateInRange({ 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_APRIL, 2008 },
                       { 9, MONTH_JANUARY, 2009 }) == false);

    assert(dateInRange({ 10, MONTH_JANUARY, 2009 },
                       { 10, MONTH_APRIL, 2008 },
                       { 10, MONTH_JANUARY, 2009 }) == true);

    try {
        assert(dateInRange({ 10, 2009 },
                           { 10, MONTH_APRIL, 2008 },
                           { 9, MONTH_JANUARY, 2009 }) == false);
        return 6;
    } catch (std::invalid_argument e) {
        // All is ok
    }
    try {
        assert(dateInRange({ 10, MONTH_JANUARY, 2009 },
                           { 10, MONTH_APRIL, 2010 },
                           { 9, MONTH_JANUARY, 2009 }) == false);
        return 7;
    } catch (std::invalid_argument e) {
        // All is ok
    }

    cout << "Testing Date Utils : dateInRange : Ended\n";

    cout << "Testing Date Utils : zodiakByDate\n";

    assert(zodiakByDate({ 15, MONTH_APRIL, 2009 }) == ZODIAK_ARIES);
    assert(zodiakByDate({ 15, MONTH_MAY, 2009 }) == ZODIAK_TAURUS);
    assert(zodiakByDate({ 15, MONTH_JUNE, 2009 }) == ZODIAK_GEMINI);
    assert(zodiakByDate({ 15, MONTH_JULE, 2009 }) == ZODIAK_CANCER);
    assert(zodiakByDate({ 15, MONTH_AUGUST, 2009 }) == ZODIAK_LEO);
    assert(zodiakByDate({ 15, MONTH_SEPTEMBER, 2009 }) == ZODIAK_VIRGO);
    assert(zodiakByDate({ 15, MONTH_OCTOBER, 2009 }) == ZODIAK_LIBRA);
    assert(zodiakByDate({ 15, MONTH_NOVEMBER, 2009 }) == ZODIAK_SCORPIO);
    assert(zodiakByDate({ 15, MONTH_DECEMBER, 2009 }) == ZODIAK_SAGGITARIUS);
    assert(zodiakByDate({ 29, MONTH_DECEMBER, 2009 }) == ZODIAK_CAPRICON);
    assert(zodiakByDate({ 15, MONTH_JANUARY, 2009 }) == ZODIAK_CAPRICON);
    assert(zodiakByDate({ 15, MONTH_FEBRUARY, 2009 }) == ZODIAK_AQUARIUS);
    assert(zodiakByDate({ 15, MONTH_MARCH, 2009 }) == ZODIAK_PISCES);

    try {
        assert(zodiakByDate({ 29, MONTH_FEBRUARY, 2009 }) == ZODIAK_SCORPIO);
        return 8;
    } catch (std::invalid_argument e) {
        // All is ok
    }

    cout << "Testing Date Utils : zodiakByDate : Ended\n";

    return 0;
}

int
checkDatabase()
{
    cout << "Checking database" << endl;

    Database db("notelist_debug.db");

    db.Purge();

    //Добавление и запрос
    db.addNote("Иван Иванович", ZODIAK_LEO, { 19, MONTH_JANUARY, 1999 });
    NoteList request;
    db.Get(request);
    assert(request.size() == 1);

    //Сортировка
    db.addNote("Алах Иванович", ZODIAK_LIBRA, { 19, MONTH_JANUARY, 2016 });
    db.Get(request, ZnakFieldID_FULLNAME);
    String firstElement = request[0].fullname;
    assert(firstElement.compare("Алах Иванович") == 0);

    //Проверка наличия
    assert(db.contains("Алах Иванович", { 19, MONTH_JANUARY, 2016 }) == true);
    assert(db.contains(" Иванович", { 19, MONTH_JANUARY, 2016 }) == false);

    //Редактирование
    NoteID noteid = request[0].id;
    String new_fullname = "ЭП2к";
    db.editNote(noteid, &new_fullname);
    db.Get(request);
    assert(request[1].fullname.compare("ЭП2к") == 0);

    Zodiak new_znak = ZODIAK_AQUARIUS;
    db.editNote(noteid, &new_fullname, &new_znak);
    db.Get(request);
    assert(request[1].fullname.compare("ЭП2к") == 0 &&
           request[1].znak == ZODIAK_AQUARIUS);

    Date new_date = { 3, MONTH_FEBRUARY, 2006 };
    db.editNote(noteid, &new_fullname, &new_znak, &new_date);
    db.Get(request);
    assert(request[1].fullname.compare("ЭП2к") == 0 &&
           request[1].znak == ZODIAK_AQUARIUS &&
           dateToString(request[1].date) == "2006-2-3");

    //Поиск

    db.Search(request, ZnakFieldID_FULLNAME, "ЭП2к");
    assert(request.size() == 1);
    request.clear();

    db.Search(request, ZnakFieldID_ZNAK, std::to_string(ZODIAK_AQUARIUS));
    assert(request.size() == 1);
    request.clear();

    db.Search(
      request, ZnakFieldID_DATE, dateToString({ 3, MONTH_FEBRUARY, 2006 }));
    assert(request.size() == 1);

    //Удаление
    db.removeNote(noteid);
    db.Get(request);
    assert(request.size() == 1);

    db.Purge();
    db.Get(request);
    assert(request.size() == 0);

    std::cout << "Checking database : Ended" << std::endl;

    return 0;
}

#ifndef NOTELIST_MAINWINDOW
#define NOTELIST_MAINWINDOW

#include "database.hpp"
#include "noteListModel.hpp"
#include "ui_mainwindow.h"
#include <QtWidgets/QtWidgets>

class MainWindow : public QWidget
{
    Q_OBJECT

  private:
    Ui::mainWindow* m_ui;
    NoteListModel* m_notes;
    Database m_db;
    struct
    {
        bool searching = false;
        String request = "";
        ZnakFieldIDs field = ZnakFieldID_ID;

    } m_searchRequest;

  public:
    MainWindow(QWidget* parent = nullptr);
    MainWindow(const MainWindow& other) = delete;
    ~MainWindow();

  public slots:
    void addButtonPessed();
    void editButtonPressed();
    void helpButtonPressed();
    void removeButtonPessed();
    void sortComboBoxIndexChanged(int index);
    void sortCheckBoxChanged(int state);
    void searchButtonPressed();
    void purgeDBButtonPressed();

  private:
    void update();
};

#endif //! NOTELIST_MAINWINDOW
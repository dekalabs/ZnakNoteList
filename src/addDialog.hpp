#ifndef NOTELIST_ADDDIALOG
#define NOTELIST_ADDDIALOG

#include "noteznak.hpp"
#include "ui_editForm.h"
#include <QtWidgets/QtWidgets>

class AddDialog : public QDialog
{
    Q_OBJECT
  private:
    Ui::editForm* m_ui;
    ZNAK& m_note;
    ZNAK m_startNote;

  public:
    AddDialog(ZNAK& noteToEdit, QWidget* parent = nullptr);
    AddDialog(const AddDialog& other) = delete;
    ~AddDialog();

    bool validateInput();

  public slots:
    void continueButtonClicked();
    void helpButtonClicked();
    void dateChanged(const QDate& date);
};

#endif //! NOTELIST_ADDDIALOG

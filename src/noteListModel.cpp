#include "noteListModel.hpp"

NoteListModel::NoteListModel(QObject* parent)
  : QAbstractTableModel(parent)
  , m_znaks()
{}

QVariant
NoteListModel::data(const QModelIndex& index, int Role) const
{
    if (!index.isValid())
        return QVariant();

    if (Role == Qt::DisplayRole &&
        static_cast<size_t>(index.row()) < m_znaks.size() &&
        index.column() < ZnakFieldID_MAX) {
        switch (index.column()) {
            case ZnakFieldID_ID:
                return m_znaks[index.row()].id;
            case ZnakFieldID_FULLNAME:
                return QString::fromStdString(m_znaks[index.row()].fullname);
            case ZnakFieldID_ZNAK:
                return QString::fromStdString(
                  znakNames[m_znaks[index.row()].znak]);
            case ZnakFieldID_DATE:
                return QString::fromStdString(
                  dateToString(m_znaks[index.row()].date));
            default:
                return QVariant();
        }
    }
    return QVariant();
}

int
NoteListModel::rowCount(const QModelIndex& parent) const
{
    (void)parent;
    return static_cast<int>(m_znaks.size());
}

QVariant
NoteListModel::headerData(int section,
                          Qt::Orientation orientation,
                          int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case ZnakFieldID_ID:
                return "Идентификатор";
            case ZnakFieldID_FULLNAME:
                return "Фамилия, имя";
            case ZnakFieldID_ZNAK:
                return "Знак";
            case ZnakFieldID_DATE:
                return "Дата рождения";
            default:
                return "";
        }
    } else {
        return QVariant();
    }
}

void
NoteListModel::UpdateList(const NoteList& list)
{
    // Force update table
    this->beginResetModel();

    m_znaks = list;
    QModelIndex topLeft = this->index(0, 0);
    QModelIndex bottomRight =
      this->index(static_cast<int>(m_znaks.size()), columnCount());
    emit dataChanged(topLeft, bottomRight);

    this->endResetModel();
}

#include "searchDialog.hpp"
#include "database.hpp"
#include <regex>

SearchDialog::SearchDialog(QWidget* parent)
  : QDialog(parent)
  , m_ui(nullptr)
  , m_request("")
  , m_field(ZnakFieldID_ID)
{
    m_ui = new Ui::searchwindow();
    m_ui->setupUi(this);

    connect(m_ui->pushButton_continue,
            SIGNAL(clicked()),
            this,
            SLOT(continueButtonClicked()));

    connect(m_ui->pushButton_help,
            SIGNAL(clicked()),
            this,
            SLOT(helpButtonClicked()));
}

SearchDialog::~SearchDialog()
{
    delete m_ui;
}

String
SearchDialog::GetRequest()
{
    return m_request;
}

ZnakFieldIDs
SearchDialog::GetField()
{
    return m_field;
}

bool
SearchDialog::validateInput()
{
    String request = m_ui->lineEdit_request->text().toStdString();
    if (request.size() == 0)
        return false;
    ZnakFieldIDs field =
      static_cast<ZnakFieldIDs>(m_ui->comboBox_field->currentIndex());
    m_field = field;
    if (field == ZnakFieldID_ID) {
        auto regex = std::regex("\\d+"); //< Только цифры могут быть в индексе
        std::smatch match;
        std::regex_match(request, match, regex);
        if (match.empty())
            return false;
    } else if (field == ZnakFieldID_FULLNAME) {
        if (request.size() > MAX_CHARS_FULLNAME) {
            return false;
        }
    } else if (field == ZnakFieldID_ZNAK) {
        for (uint zodiak = ZODIAK_BEGIN; zodiak < ZODIAK_MAX; zodiak++) {
            QString zodiakName = QString(znakNames[zodiak]).toLower();
            QString request_lower = QString::fromStdString(request).toLower();
            if (request_lower.compare(zodiakName) == 0) {
                m_request = std::to_string(zodiak);
                return true;
            }
        }
        return false;
    } else if (field == ZnakFieldID_DATE) {
        Date date;
        try {
            date = stringToDate(request);
            if (validateDate(date)) {
                m_request = dateToString(date); //< Убираем незначащие нули
                return true;
            } else
                return false;
        } catch (const std::exception& except) {
            (void)except;
            return false;
        }

    } else if (field == ZnakFieldID_DAY) {
        try {
            int day = std::stoi(request);
            if (day < 1 || day > 31)
                return false;
            m_request = std::to_string(day); //< Убираем незначащие нули
        } catch (const std::exception& except) {
            (void)except;
            return false;
        }
    } else if (field == ZnakFieldID_MONTH) {
        try {
            int month = std::stoi(request);
            if (month < 1 || month > 12)
                return false;
            m_request = std::to_string(month); //< Убираем незначащие нули
        } catch (const std::exception& except) {
            (void)except;
            return false;
        }
    } else if (field == ZnakFieldID_YEAR) {
        try {
            int year = std::stoi(request);
            if (year < 0)
                return false;
            m_request = std::to_string(year); //< Убираем незначащие нули
        } catch (const std::exception& except) {
            (void)except;
            return false;
        }
    }
    m_request = request;
    return true;
}

void
SearchDialog::continueButtonClicked()
{
    if (validateInput()) {
        m_field =
          static_cast<ZnakFieldIDs>(m_ui->comboBox_field->currentIndex());

        this->accept();
    } else {
        this->setModal(false);
        QMessageBox::warning(this,
                             "Ошибка при вводе",
                             "Вы ввели неверные данные, нажмите помощь, чтобы "
                             "узнать требования для корректного запроса.");
        this->setModal(true);
    }
}

void
SearchDialog::helpButtonClicked()
{
    QString str =
      "В этом окне вы можете ввести запрос на поиск данных в базе данных.\n\n"
      "Условия корректного ввода:\n\t"
      "Идентификатор должен быть числом.\n\t"
      "Длина фамилии и имени не должны превышать %1 символов.\n\t"
      "Знак зодиака должен быть написан без ошибок, регистр букв значения не "
      "имеет.\n\t"
      "Дата имеет формат: ГГГГ-ММ-ДД\n\t"
      "День, месяц, год рождения - должны быть числом в допустимых "
      "пределах.\n\n\t"

      "В поле имени можно использовать шаблон для поиска: \n\t\t"
      "Используйте % чтобы указать, что в этом месте должно быть любое "
      "количество символов.\n\t\t"
      "Символ _ обозначает, что должна находится 1 буква тут.\n\t";
    str = str.arg(MAX_CHARS_FULLNAME);
    this->setModal(false);
    QMessageBox::information(this, "Информация", str);
    this->setModal(true);
}

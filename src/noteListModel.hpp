#ifndef NOTELIST_NOTELISTMODEL
#define NOTELIST_NOTELISTMODEL

#include "noteznak.hpp"
#include <QtCore/QAbstractTableModel>

class NoteListModel : public QAbstractTableModel
{
    Q_OBJECT
  private:
    NoteList m_znaks;

  public:
    NoteListModel(QObject* parent = nullptr);

    QVariant data(const QModelIndex& index, int Role) const;

    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& parent = QModelIndex()) const
    {
        return 3; //< 3 столбца: имя, зодиак, дата рождения
    }

    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    void UpdateList(const NoteList& list);
    ZNAK& Get(int row) { return m_znaks[row]; };
};

#endif //! NOTELIST_NOTELISTMODEL

#include "mainWindow.hpp"
#include <QtWidgets/QtWidgets>

int
main(int argc, char* argv[])
{
    int code = 0;
    QApplication app(argc, argv);

    try {
        MainWindow wnd;
        wnd.show();

        code = app.exec();
    } catch (const std::exception& ex) {
        QMessageBox::critical(nullptr, "Критическая ошибка", ex.what());
        QMessageBox::information(
          nullptr,
          "Помощь",
          "Если ошибка связана с отсуствием доступа или другим локальным "
          "причинам, вы можете её исправить. В других случаях, следует "
          "рассказать о проблеме разработчику.");
        code = -1;
    }
    return code;
}

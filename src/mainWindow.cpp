#include "mainWindow.hpp"
#include "addDialog.hpp"
#include "searchDialog.hpp"

MainWindow::MainWindow(QWidget* parent)
  : QWidget(parent)
  , m_ui(nullptr)
  , m_notes(nullptr)
  , m_db()
{
    m_ui = new Ui::mainWindow();
    m_ui->setupUi(this);
    m_notes = new NoteListModel(this);

    m_ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(
      m_ui->pushButton_add, SIGNAL(clicked()), this, SLOT(addButtonPessed()));

    connect(m_ui->pushButton_edit,
            SIGNAL(clicked()),
            this,
            SLOT(editButtonPressed()));

    connect(m_ui->pushButton_delete,
            SIGNAL(clicked()),
            this,
            SLOT(removeButtonPessed()));

    connect(m_ui->pushButton_purge,
            SIGNAL(clicked()),
            this,
            SLOT(purgeDBButtonPressed()));

    connect(m_ui->pushButton_search,
            SIGNAL(clicked()),
            this,
            SLOT(searchButtonPressed()));

    connect(m_ui->pushButton_help,
            SIGNAL(clicked()),
            this,
            SLOT(helpButtonPressed()));

    connect(m_ui->comboBox_sorting,
            SIGNAL(currentIndexChanged(int)),
            this,
            SLOT(sortComboBoxIndexChanged(int)));

    connect(m_ui->checkBox_sort_descend,
            SIGNAL(stateChanged(int)),
            this,
            SLOT(sortCheckBoxChanged(int)));

    //Обновлние таблицы
    NoteList notelist;
    m_db.Get(notelist);
    m_notes->UpdateList(notelist);
    m_ui->tableView->setModel(m_notes);
}

MainWindow::~MainWindow()
{
    delete m_ui;
    delete m_notes;
}

void
MainWindow::addButtonPessed()
{
    ZNAK new_znak;
    int result = QDialog::Rejected;
    do {

        AddDialog dialog(new_znak, this);
        dialog.exec();
        result = dialog.result();
        if (result == QDialog::Rejected)
            break;
        if (result == QDialog::Accepted &&
            m_db.contains(new_znak.fullname, new_znak.date)) {
            QMessageBox::information(
              this,
              "Повторение элемента",
              "Такой элемент уже есть, повторите ввод значений.");
        } else
            break;
    } while (true);
    if (result == QDialog::Accepted) {
        m_db.addNote(new_znak.fullname, new_znak.znak, new_znak.date);
    }

    update();
}

void
MainWindow::editButtonPressed()
{
    auto index = m_ui->tableView->currentIndex();
    if (index.isValid()) {
        ZNAK oldZnak = m_notes->Get(index.row());
        ZNAK znak = oldZnak;
        int result = QDialog::Rejected;
        do {
            AddDialog dialog(znak, this);
            result = dialog.exec();
            if (result == QDialog::Accepted &&
                m_db.contains(znak.fullname, znak.date)) {
                QMessageBox::information(
                  this,
                  "Повторение элемента",
                  "Такой элемент уже есть, повторите ввод значений.");
                znak = oldZnak;
            } else
                break;
        } while (true);
        if (result == QDialog::Accepted)
            m_db.editNote(znak.id, &znak.fullname, &znak.znak, &znak.date);
    } else {
        QMessageBox::warning(
          this, "Ошибка", "Для редактирования выберите в списке элемент.");
    }
    update();
}

void
MainWindow::helpButtonPressed()
{
    QString text =
      "Эта программа позволяет сохранять дату рождения и знак зодиака для "
      "людей, чьё имя и фамилию вы указали.\n\n\n"
      "Информация по управлению:\n\t"
      "Для добавления информации нажмите \"Добавить\"\n\t"
      "Для изменения записи выберите её в списке и нажмите \"Изменить\"\n\t"
      "Чтобы удалить запись, нажмите \"Удалить\"\n\t"
      "Используйте выпадающий список для сортировки по значению столбца.\n\t"
      "Установите галочку, если хотите сортировку в сторону убывания\n\t"
      "Нажмите \"Искать\" для поиска интересующих вас записей.\n\t"
      "Если вам необходимо удалить все записи нажмите \"Очистить БД\"";

    QMessageBox::information(this, "Помощь", text);
}

void
MainWindow::removeButtonPessed()
{
    auto index = m_ui->tableView->currentIndex();
    if (index.isValid()) {
        if (QMessageBox::question(
              this,
              "Удаление",
              "Вы уверены, что хотите удалить этот элемент?") ==
            QMessageBox::Yes) {
            ZNAK& znak = m_notes->Get(index.row());
            m_db.removeNote(znak.id);
        }
    } else {
        QMessageBox::warning(
          this, "Ошибка", "Выберите элемент перед удалением.");
    }

    update();
}

void
MainWindow::sortComboBoxIndexChanged(int index)
{
    (void)index;
    update();
}

void
MainWindow::sortCheckBoxChanged(int state)
{
    (void)state;
    update();
}

void
MainWindow::searchButtonPressed()
{
    if (m_searchRequest.searching) {
        m_ui->pushButton_search->setText("Поиск");
        m_searchRequest.searching = false;
        update();

    } else {

        if (m_notes->rowCount() == 0) { //Нет элементов
            QMessageBox::information(
              this, "Поиск", "База данных пуста; поиск не даст результатов.");
            return;
        }

        SearchDialog dialog(this);
        int result = dialog.exec();
        if (result == QDialog::Accepted) {
            m_ui->pushButton_search->setText("Отмена поиска");
            m_searchRequest.request = dialog.GetRequest();
            m_searchRequest.field = dialog.GetField();
            m_searchRequest.searching = true;

            update();

            if (m_notes->rowCount() == 0) {
                QMessageBox::information(
                  this, "Поиск", "Поиск не дал результатов.");
            }
        }
    }
}

void
MainWindow::purgeDBButtonPressed()
{
    if (QMessageBox::question(
          this, "Удаление", "Вы уверены, что хотите удалить все записи?") ==
        QMessageBox::No) {
        return;
    }
    m_db.Purge();
    update();
}

void
MainWindow::update()
{
    int index = m_ui->comboBox_sorting->currentIndex();
    bool isDescending = m_ui->checkBox_sort_descend->isChecked();
    NoteList list;
    if (m_searchRequest.searching) {
        m_db.Search(list,
                    m_searchRequest.field,
                    m_searchRequest.request,
                    static_cast<ZnakFieldIDs>(index),
                    isDescending);
    } else {
        m_db.Get(list, static_cast<ZnakFieldIDs>(index), isDescending);
    }
    m_notes->UpdateList(list);
}

#ifndef NOTELIST_SEARCHDIALOG
#define NOTELIST_SEARCHDIALOG

#include "noteznak.hpp"
#include "ui_searchwindow.h"
#include <QtWidgets/QtWidgets>

class SearchDialog : public QDialog
{
    Q_OBJECT

  private:
    Ui::searchwindow* m_ui;
    String m_request;
    ZnakFieldIDs m_field;

  public:
    SearchDialog(QWidget* parent = nullptr);
    SearchDialog(const SearchDialog& other) = delete;
    ~SearchDialog();

    String GetRequest();
    ZnakFieldIDs GetField();

    bool validateInput();

  public slots:
    void continueButtonClicked();
    void helpButtonClicked();
};

#endif //! NOTELIST_SEARCHDIALOG

#include "addDialog.hpp"
#include "database.hpp"

AddDialog::AddDialog(ZNAK& noteToEdit, QWidget* parent)
  : QDialog(parent)
  , m_ui(nullptr)
  , m_note(noteToEdit)
  , m_startNote(noteToEdit)
{
    m_ui = new Ui::editForm();
    m_ui->setupUi(this);

    this->setModal(true);

    m_ui->lineEdit_fullname->setText(QString::fromStdString(m_note.fullname));
    m_ui->comboBox->setCurrentIndex(m_note.znak);
    m_ui->dateEdit->setDate(QDate(static_cast<int>(m_note.date[DATE_YEAR]),
                                  static_cast<int>(m_note.date[DATE_MONTH] + 1),
                                  static_cast<int>(m_note.date[DATE_DAY])));

    connect(m_ui->pushButton_continue,
            SIGNAL(clicked()),
            this,
            SLOT(continueButtonClicked()));

    connect(m_ui->pushButton_help,
            SIGNAL(clicked()),
            this,
            SLOT(helpButtonClicked()));

    connect(m_ui->dateEdit,
            SIGNAL(dateChanged(const QDate&)),
            this,
            SLOT(dateChanged(const QDate&)));
}

AddDialog::~AddDialog()
{
    this->setModal(false);
    delete m_ui;
}

bool
AddDialog::validateInput()
{
    QPalette pallete_normal = m_ui->lineEdit_fullname->palette();
    pallete_normal.setColor(QPalette::Base, QColor(qRgb(255, 255, 255)));
    QPalette pallete_errored = m_ui->lineEdit_fullname->palette();
    pallete_errored.setColor(QPalette::Base, QColor(qRgb(255, 0, 0)));

    bool output = true;

    // Checking name
    auto name = m_ui->lineEdit_fullname->text();
    if (name.isEmpty() || name.size() > MAX_CHARS_FULLNAME) {
        m_ui->lineEdit_fullname->setPalette(pallete_errored);
        output = false;
    } else {
        m_ui->lineEdit_fullname->setPalette(pallete_normal);
        // Checking comboBox
        if (m_ui->comboBox->currentIndex() == -1) {
            m_ui->comboBox->setPalette(pallete_errored);
            output = false;
        } else {
            m_ui->comboBox->setPalette(pallete_normal);
        }
    }

    return output;
}

void
AddDialog::continueButtonClicked()
{
    if (!validateInput()) {
        this->setModal(false);
        QMessageBox::warning(this,
                             "Ошибка при вводе",
                             "Вы должны ввести корректные данные.\nНе верно "
                             "введенные данные отмечены красным цветом");
        this->setModal(true);
    } else {
        // Commiting changes
        auto name = m_ui->lineEdit_fullname->text();
        m_note.fullname = name.toStdString();

        m_note.znak = static_cast<Zodiak>(m_ui->comboBox->currentIndex());

        auto dateStr =
          m_ui->dateEdit->date().toString("yyyy-M-d").toStdString();
        m_note.date = stringToDate(dateStr);

        if (m_note.fullname == m_startNote.fullname &&
            dateCompare(m_note.date, m_startNote.date) == 0) {
            this->reject();
            return;
        }

        this->accept();
    }
}

void
AddDialog::helpButtonClicked()
{
    QString str =
      "В этом окне вы можете добавить или редактировать существующую запись.\n"
      "Чтобы ввод считался коректным необходимо: \n\t"
      "* Строка \"Фамилия, имя\" не пустая и не превышает %1 символов.\n\t"
      "* Выбран 1 элемент в выпадающем списке.\n\t"
      "* Дата выбрана в календаре";
    str = str.arg(MAX_CHARS_FULLNAME);
    this->setModal(false);
    QMessageBox::information(this, "Информация", str);
    this->setModal(true);
}

void
AddDialog::dateChanged(const QDate& date)
{
    auto dateStr = date.toString("yyyy-M-d").toStdString();
    Date new_date = stringToDate(dateStr);

    Zodiak new_zodiak = zodiakByDate(new_date);
    m_ui->comboBox->setCurrentIndex(new_zodiak);
}

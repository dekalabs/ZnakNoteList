# ZnakNoteList
It's useless project. Just expirements.

## Description
The project allows to keep people's fullname, zodiak, day of birth.

## Getting sources

The simplest way to get sources is downloading from github in .zip.
You can download the code [here](https://github.com/SpeedCrash100/ZnakNoteList/archive/master.zip)

Or you can use [git](https://git-scm.com/) to download and update the source code.
When that's installed, make sure that you have git in your PATH variable.
Open terminal or cmd and type:

    git clone https://github.com/SpeedCrash100/ZnakNoteList.git
    
To update the code use:

    git pull

## Building 

### Prerequisites
 * [CMake](https://cmake.org/download/)
 * [Qt](https://www.qt.io/download)
 * [SQLite3 C/C++ API](https://www.sqlite.org/download.html)

### Configuring
To configure project you should enter in build directory and type in terminal:

    cmake <path-to-source>
    
If you receiving error, follow with a cmake instruction. 
### Building
In terminal type

    cmake --build <path-to-build-dir>
    
Now you can run the application.



THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
